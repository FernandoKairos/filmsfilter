  export class FilterBase {
      constructor(prop) {
          this.prop = prop;
          this._key = "";
          this.regExp = new RegExp();
      }
      //key es el valor de filtro actual
      set key(value) {
          this._key = value;
          if (value) {
              this.regExp = new RegExp(value);
          } else {
              this.regExp = "";
          }
      }

      get key() {
          return this._key;
      }

      //una movie pasa el filter si la key que tiene en ese momento es la del filtro deseado
      filter(movie) {
          if (this._key) {
              return movie[this.prop].match(this.regExp);
          } else {
              return true;
          }
      }
  }
  export class FilterMain extends FilterBase {
      constructor() {
          super("");
      }

      filter(movie) {
          if (this.key) {
              let movieText = [
                  movie.title,
                  movie.description,
                  movie.director,
                  movie.producer,
                  movie.release_date,
                  movie.rt_score
              ].join(" ").toLowerCase();
              return movieText.match(this.regExp);
          } else {
              return true;
          }
      }

  }
  export  class FilterTitle extends FilterBase {
      constructor() {
          super("title");
      }
  }
  export class FilterDescription extends FilterBase {
      constructor() {
          super("description");
      }
  }
  export class FilterDirector extends FilterBase {
      constructor() {
          super("director");
      }
  }
  export class FilterProducer extends FilterBase {
      constructor() {
          super("producer");
      }
  }
  export  class FilterReleaseDate extends FilterBase {
      constructor() {
          super("release_date");
      }
  }
  export  class FilterRtScore extends FilterBase {
      constructor() {
          super("rt_score");
      }
  }
