//Main structure for tests.
// describe('method', () => {
//     test('test something', () => {
//     expect(true).toBeTruthy()
//     })
//     })

// describe("",() => {
//     test("Test someting", () => {
//         expect(true).toBeTruthy()
//     }
// )});

const TestRunner = require("jest-runner")
import {resetAllFilters} from './main.js';
import {showLoaderGif} from './main.js';
import {filters} from './main.js';
import {div_film} from './main.js';
import {result} from './fimls.ficture.js';
import {setFilms} from './main.js';
import {getPictureSrc} from './main.js';

//#region Unitary Tests
test('ShowLoaderGif set to its style.display to none', () => {
    let loader = `<img id="loader" src="../assets/loader/loader.gif">`;
    document.body.innerHTML = loader;
    const loaderGif = document.querySelector("#loader");
    showLoaderGif(loaderGif);
    let card = document.querySelector("#loader").style.display;
    expect(loaderGif.style.display).toBe(card);
});

test('getPictureSrc should replace empty spaces to _ and add .jpg to the end', () => {
    let movie = {"id": "2baf70d1-42bb-4437-b551-e5fed5a87abe",
    "title": "Castle in the Sky",
    "description": "The orphan Sheeta inherited a mysterious crystal that links her to the mythical sky-kingdom of Laputa. With the help of resourceful Pazu and a rollicking band of sky pirates, she makes her way to the ruins of the once-great civilization. Sheeta and Pazu must outwit the evil Muska, who plans to use Laputas science to make himself ruler of the world.",
    "director": "Hayao Miyazaki",
    "producer": "Isao Takahata",
    "release_date": "1986",
    "rt_score": "95",
    "people": ["https://ghibliapi.herokuapp.com/people/"],
    "species": ["https://ghibliapi.herokuapp.com/species/af3910a6-429f-4c74-9ad5-dfe1c4aa04f2"],
    "locations": ["https://ghibliapi.herokuapp.com/locations/"],
    "vehicles": ["https://ghibliapi.herokuapp.com/vehicles/"],
    "url": "https://ghibliapi.herokuapp.com/films/2baf70d1-42bb-4437-b551-e5fed5a87abe"
};
 let result = getPictureSrc(movie);
    expect(result).toBe("../assets/filmsCoverts/Castle_in_the_Sky.jpg");
});

//#endregion

//#region Integration Tests

//#endregion

function resetAllFiltersTest() {
    resetAllFilters();
    let result = true;
    if (result) {
        result = (document.getElementById("inputSearch").value == "");
    }
    if (result) {
        const div_film = document.querySelector("#filmSelector");
        result = (div_film.innerHTML == "");
    }
    if (result) {
        const loaderGif = document.querySelector("#loader");
        result = (loaderGif.style.display == "none");
    }
    if (result) {
        for (let filter of filters) {
            result = (filter.key == "");
            if (!result) {
                break;
            }
        }
    }
    return result;
}

describe('When called succesfully', () => {
    let mockGetFilms;
    //beforeAll solo limpia la function getfilms();
    beforeAll((done) => {
        //jest.fn es el spy
        mockGetFilms = jest.fn(() => {
            return Promise.resolve(result);
        })
        //cierre del ciclo + limpieza
        done();
    });
    //caso espefcifico
    it('should return result', done => {
        mockGetFilms()
            .then((data) => {
                //comparo data con el mock
                expect(data).toBe(result);
                done();
            })
            .catch(() => {
                done.fail("ERROR EN FETCH");
            });
    });
});

//TODO: ask cristina about jsdom because how can i have access to the DOM elements?