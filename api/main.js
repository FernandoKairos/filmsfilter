
const http = require('http');
const express = require('express');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

function getJSON(endpoint) {
    return new Promise((resolve, reject) => {
        http.get(endpoint, (response) => {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                try {
                    data = JSON.parse(data);
                    resolve(data);
                } catch (error) {
                    reject(error);
                }
            });
        }).on('error', reject);
    });
}

getJSON('http://ghibliapi.herokuapp.com/films/').then(console.log, console.error);


app.get('/',(req,res) => res.render('../view/index'));
app.get('/:id',(req,res) => res.send("Hy moovie!"));


app.listen(5000, () => console.log('Example app listening on port 5000'));
