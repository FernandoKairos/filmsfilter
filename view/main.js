'use strict'
//#region  Imports
import {FilterBase} from './filters.js';
import {FilterMain} from './filters.js';
import {FilterTitle} from './filters.js';
import {FilterDescription} from './filters.js';
import {FilterDirector} from './filters.js';
import {FilterProducer} from './filters.js';
import {FilterReleaseDate} from './filters.js';
import {FilterRtScore} from './filters.js';
//#endregion 
//#region Instances
const filterBase = new FilterBase();
const filterMain = new FilterMain();
const filterTitle = new FilterTitle();
const filterDescription = new FilterDescription();
const filterDirector = new FilterDirector();
const filterProducer = new FilterProducer();
const filterReleaseDate = new FilterReleaseDate();
const filterRtScore = new FilterRtScore();
//#endregion

const loaderGif = document.querySelector("#loader");
export const div_film = document.querySelector("#filmSelector");
const inputSearch = document.getElementById("inputSearch");
var films = [];

export function resetAllFilters() {
    inputSearch.value = "";
    filterByTitle("");
    filterByDirector("");
    filterByProducer("");
    filterByReleaseDate("");
    filterByRtScore("");
    resfreshFilms();
    div_film.innerHTML = "";
    showLoaderGif(loaderGif);
    for (let movie of films) {
        div_film.appendChild(createFilmCard(movie));
    }
}
export function resfreshFilms() {
    div_film.innerHTML = "";
    div_film.innerHTML.toLowerCase();
    for (let film of films) {
        if (filterFilm(film)) {
            div_film.appendChild(createFilmCard(film));
            showLoaderGif(loaderGif);
        }
    }
    // films.map((movie) => {
    //     //Al ser result = true, todos los filtros son ciertos hasta que se demuestre lo contrario.
    //     let result = true;
    //     if (result) result = filterMain.filter(movie);
    //     if (result) result = filterTitle.filter(movie);
    //     if (result) result = filterDescription.filter(movie);
    //     if (result) result = filterDirector.filter(movie);
    //     if (result) result = filterProducer.filter(movie);
    //     if (result) result = filterReleaseDate.filter(movie);
    //     if (result) result = filterRtScore.filter(movie);
    //     if (result) {
    //         div_film.appendChild(createFilmCard(movie));
    //         loaderGif.style.display = "none";
    //     }
    //});
}
export function showLoaderGif(loaderGif) {
    loaderGif.style.display = "none";
}
export function refreshMenu(films, prop, functionFilterName, menuNode){
    menuNode.innerHTML = "";
    var itemNode = document.createElement("a");
    itemNode.setAttribute("class", "dropdown-item");
    itemNode.setAttribute("href", "#");
    itemNode.setAttribute("onclick", `${functionFilterName}('')`);
    itemNode.textContent = "No Filter";
    menuNode.appendChild(itemNode);
    var dict = {};
    for (let film of films) {
        dict[film[prop]] = true;
    }
    for (let name in dict) {
        itemNode = document.createElement("a");
        itemNode.setAttribute("class", "dropdown-item");
        itemNode.setAttribute("href", "#");
        itemNode.setAttribute("onclick", `${functionFilterName}('${name}')`);
        itemNode.textContent = name;
        menuNode.appendChild(itemNode);
    }
}
export function getPictureSrc(movie) {
    let result = movie.title.replace(/ /g, "_");
    result = "../assets/filmsCoverts/" + result + ".jpg";
    return result;
}
export function createFilmCard(movie) {
    let divCardNode = document.createElement("div");
    divCardNode.setAttribute("class", "card col-xl-2 col-md-4 col-sm-6");
    //
    let divCardHeaderNode = document.createElement("div");
    let divCardTitleNode = document.createElement("h3");

    divCardHeaderNode.appendChild(divCardTitleNode);

    divCardHeaderNode.setAttribute("class", "card-header");
    divCardTitleNode.textContent = movie.title;
    //Card body elements

    let divCardBodyNode = document.createElement("div");
    let divCardPictureContainer = document.createElement("div");
    let divCardPicture = document.createElement("img");

    //Nesting card body elements
    divCardBodyNode.appendChild(divCardPictureContainer);
    divCardPictureContainer.appendChild(divCardPicture);
    //setting attributes of elements
    divCardBodyNode.setAttribute("class", "card-body");
    divCardBodyNode.setAttribute("class", "flex:d-flex align-items-center")
    divCardPictureContainer.setAttribute("class", "text-center");
    divCardPicture.setAttribute("src", getPictureSrc(movie));
    divCardPicture.setAttribute("height", "250");
    divCardPicture.setAttribute("width", "166");

    //
    let divCardFooterNode = document.createElement("div");
    let divCardDescriptionNode = document.createElement("p");
    let divCardDirectorNode = document.createElement("p");
    let divCardProducerNode = document.createElement("p");
    let divCardRelease_dateNode = document.createElement("p");
    let divCardRt_scoreNode = document.createElement("p");

    divCardFooterNode.appendChild(divCardDescriptionNode);
    divCardFooterNode.appendChild(divCardDirectorNode);
    divCardFooterNode.appendChild(divCardProducerNode);
    divCardFooterNode.appendChild(divCardRelease_dateNode);
    divCardFooterNode.appendChild(divCardRt_scoreNode);

    divCardDescriptionNode.textContent = movie.description;
    divCardDirectorNode.textContent = movie.director;
    divCardProducerNode.textContent = movie.producer;
    divCardRelease_dateNode.textContent = movie.release_date;
    divCardRt_scoreNode.textContent = movie.rt_score;
    divCardFooterNode.setAttribute("class", "card-footer");
    divCardDirectorNode.setAttribute("class", "font-weight-bold");
    divCardDescriptionNode.setAttribute("class", "small");
    //
    divCardNode.appendChild(divCardHeaderNode);
    divCardNode.appendChild(divCardBodyNode);
    divCardNode.appendChild(divCardFooterNode);
    return divCardNode;
}

//#region Filters
export function filterFilm(movie) {
    let filters = [
        filterMain,
        filterTitle,
        filterDescription,
        filterDirector,
        filterProducer,
        filterReleaseDate,
        filterRtScore
    ];
    for (let filter of filters) {
        if (!filter.filter(movie)) {
            return null;
        }
    }
    return movie;
}
export function refreshFilters() {
    refreshMenu(films, "title", "filterByTitle", document.getElementById("dropDownTitles"));
    refreshMenu(films, "director", "filterByDirector", document.getElementById("dropDownDirectors"));
    refreshMenu(films, "producer", "filterByProducer", document.getElementById("dropDownProducers"));
    refreshMenu(films, "release_date", "filterByReleaseDate", document.getElementById("dropDownReleaseDate"));
    refreshMenu(films, "rt_score", "filterByRtScore", document.getElementById("dropDownRtScores"));
}
export function filterBySearch() {
    filterMain.key = document.getElementById("inputSearch").value.toLowerCase();
    loaderGif.style.display = "block";
    resfreshFilms();
}
export function filterByTitle(title = "") {
    filterTitle.key = title;
    loaderGif.style.display = "block";
    resfreshFilms();
}
export function filterByDescription(description) {
    filterDescription.key = description;
    loaderGif.style.display = "block";
    resfreshFilms();
}
export function filterByDirector(director) {
    filterDirector.key = director;
    loaderGif.style.display = "block";
    resfreshFilms();
}
export function filterByProducer(producer) {
    filterProducer.key = producer;
    loaderGif.style.display = "block";
    resfreshFilms();
}
export function filterByReleaseDate(release_date) {
    filterReleaseDate.key = release_date;
    loaderGif.style.display = "block";
    resfreshFilms();
}
export function filterByRtScore(rtScore) {
    filterRtScore.key = rtScore;
    loaderGif.style.display = "block";
    resfreshFilms();
}
//#endregion

export function setFilms(movies) {
    films = movies;
}
export function getFilms() {
    try {
        fetch('http://ghibliapi.herokuapp.com/films/')
            .then(data => data.json())
            .then(movies => {
                setFilms(movies);
                resetAllFilters();
                refreshFilters();
                resfreshFilms();
            });
    } catch (error) {
        //console.log('oups coudln\'t get the indo from http://ghibliapi.herokuapp.com/films/ ', error);
    }
};
export let filters = [
    filterMain,
    filterTitle,
    filterDescription,
    filterDirector,
    filterProducer,
    filterReleaseDate,
    filterRtScore
];

getFilms();